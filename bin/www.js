#! /usr/bin/env node


var program = require('commander');
let path = require('path')

let config = {
    '-p,--port <val>': 'set http-serve ports',
    '-d,--dir <dir>': 'set http-server directory'
}

Object.entries(config).forEach(([key, value]) => {
    program.option(key, value)
})

program.on('--help', function () {
    console.log("-p")
    console.log("-d")
})

let obj = program.parse(process.argv) //用户传递的配置
if (!obj.dir) {
    obj['dir'] = process.cwd()
}
if (!obj.port) {
    obj['port'] = 3000
}


let Serve = require('../static-serve-sync')



let serve = new Serve(obj);
serve.start();